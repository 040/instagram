(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.mybehavior = {
    attach: function (context, settings) {
      if ($('#instafeed').length) {
        var feed = new Instafeed({
          accessToken: drupalSettings.instagram_token,
          limit: parseInt(drupalSettings.instagram_limit),
          template: '<a href="{{link}}" target="_blank"><img src="{{image}}" alt="{{caption}}" /></a>'
        });

        feed.run();
      }
    }
  };

})(jQuery, Drupal, drupalSettings);