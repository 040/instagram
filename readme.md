# Instagram

This module makes it easy to add Instagram feeds to your site. Install the module and paste your token in the text field on the Instagram settings page  found at `/admin/config/content/instagram`.

To add the feed to your page insert the following snippet info your template file.

`{{ instagram | raw }}`

curl --data branch=master "https://__token__:e-oyrSJCmHKHyLqrQa6h@gitlab.com/api/v4/projects/21923141/packages/composer"

Happy Instagraming

David @ 040
