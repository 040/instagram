<?php

namespace Drupal\instagram\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this site.
 */
class Settings extends ConfigFormBase
{
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'instagram_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'instagram.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('instagram.settings');

    $form['token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#description' => $this->t('Your Instagram API token.'),
      '#default_value' => $config->get('token'),
      '#maxlength' => 256,
    );

    $form['limit'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Limit'),
      '#description' => $this->t('Number of images to display'),
      '#default_value' => $config->get('limit'),
      '#maxlength' => 256,
    );

    $form['class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Class'),
      '#description' => $this->t('Custom CSS classes on the image wrapper'),
      '#default_value' => $config->get('class'),
      '#maxlength' => 256,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
       $this->configFactory->getEditable('instagram.settings')
      ->set('token', $form_state->getValue('token'))
      ->set('limit', $form_state->getValue('limit'))
      ->set('class', $form_state->getValue('class'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}